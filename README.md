# README #

### ULTIMATE PONG - 2P ###

* 2player pong match. 2 balls in play simultaneously. First person to reach the target points wins. Score label becomes green halfway to the target points and becomes red when player is very close to target.
* Version 1.1

### Details ###

* Programmed in Swift 3 with Spritekit
* Programmed for iOS 10.2 and the screen size is 1344x750
* Test on iPhone 6s simulator

### Contributors ###

* Souritra Das Gupta
* Original pong game idea from Jared Davidson's pong game but **every part of the code is my own, built from stuff I learned over the past few assignments**(except the way I set up the boundary. That is from Jared Davidsons app and seemed pretty cool so I used it. Its only about 3-4 lines though)

### Who do I talk to? ###

* Souritra Das Gupta
* B00671108
* sdasgup1@binghamton.edu