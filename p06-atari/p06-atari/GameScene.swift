//
//  GameScene.swift
//  p06-atari
//
//  Created by Souritra Das Gupta on 4/6/17.
//  Copyright © 2017 Souritra Das Gupta. All rights reserved.
//

import SpriteKit
import GameplayKit

var score1 = 0
var score2 = 0
var winner = 0
let scaleup = SKAction.scale(to: 1.6, duration: 0.1)
let scaledown = SKAction.scale(to: 1, duration: 0.1)
let bouncybouncy = SKAction.sequence([scaleup, scaledown])
class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let p1 = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: CGSize(width: 200, height: 20))
    let p2 = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: CGSize(width: 200, height: 20))
    //let ball = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: CGSize(width: 20, height: 20))
    //let ball2 = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: CGSize(width: 20, height: 20))
    let ball = SKSpriteNode(imageNamed: "ball")
    let ball2 = SKSpriteNode(imageNamed: "ball")
    let scorelabel1 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
    let scorelabel2 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
    
    override func didMove(to view: SKView) {
        
        
        score1 = 0
        score2 = 0
        self.physicsWorld.contactDelegate = self
        scorelabel1.text = "0"
        scorelabel1.fontSize = 25
        scorelabel1.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //scorelabel1.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        scorelabel1.position = CGPoint(x: self.size.width/2, y: self.size.height/2 - 45)
        scorelabel1.zPosition = 10
        self.addChild(scorelabel1)
        scorelabel2.text = "0"
        scorelabel2.fontSize = 25
        scorelabel2.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //scorelabel2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        scorelabel2.position = CGPoint(x: self.size.width/2, y: self.size.height/2 + 45)
        scorelabel2.zPosition = 10
        self.addChild(scorelabel2)
        ball.size = CGSize(width: 20, height: 20)
        ball2.size = CGSize(width: 20, height: 20)
        ball.physicsBody = SKPhysicsBody(circleOfRadius: 10)
        ball.position = CGPoint(x: self.frame.size.width/2 - 5, y: self.frame.size.height/2 + 7)
        ball.physicsBody?.affectedByGravity = false
        ball.physicsBody?.categoryBitMask = 2
        ball.physicsBody?.collisionBitMask = 1
        ball.physicsBody?.linearDamping = 0
        ball.physicsBody?.angularDamping = 0
        ball.physicsBody?.friction = 0
        ball.physicsBody?.restitution = 1
        self.addChild(ball)
        ball.physicsBody?.applyImpulse(CGVector(dx: 10, dy: -10))
        ball2.physicsBody = SKPhysicsBody(circleOfRadius: 10)
        ball2.position = CGPoint(x: self.frame.size.width/2 + 5, y: self.frame.size.height/2 + 7)
        ball2.physicsBody?.affectedByGravity = false
        ball2.physicsBody?.categoryBitMask = 2
        ball2.physicsBody?.collisionBitMask = 1
        ball2.physicsBody?.linearDamping = 0
        ball2.physicsBody?.angularDamping = 0
        ball2.physicsBody?.friction = 0
        ball2.physicsBody?.restitution = 1
        self.addChild(ball2)
        ball2.physicsBody?.applyImpulse(CGVector(dx: -10, dy: 10))
        // Get label node from scene and store it for use later
        /*self.label = self.childNode(withName: "//helloLabel") as? SKLabelNode
        if let label = self.label {
            label.alpha = 0.0
            label.run(SKAction.fadeIn(withDuration: 2.0))
        }
        
        // Create shape node to use during mouse interaction
        let w = (self.size.width + self.size.height) * 0.05
        self.spinnyNode = SKShapeNode.init(rectOf: CGSize.init(width: w, height: w), cornerRadius: w * 0.3)
        
        if let spinnyNode = self.spinnyNode {
            spinnyNode.lineWidth = 2.5
            
            spinnyNode.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat(M_PI), duration: 1)))
            spinnyNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                              SKAction.fadeOut(withDuration: 0.5),
                                              SKAction.removeFromParent()]))*/
        let background = SKSpriteNode(color: #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), size: self.frame.size)
        background.position = CGPoint(x: 0, y: 0)
        background.anchorPoint = CGPoint(x: 0, y: 1.0)
        addChild(background)
        let border = SKPhysicsBody(edgeLoopFrom: self.frame)
        border.friction = 0
        border.restitution = 1
        border.categoryBitMask = 3
        border.collisionBitMask = 2 | 1
        self.physicsBody = border
            p1.position = CGPoint(x: self.frame.size.width/2, y: 65)
            p2.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height - 65)
            p1.physicsBody = SKPhysicsBody(rectangleOf: p1.size)
            p2.physicsBody = SKPhysicsBody(rectangleOf: p2.size)
            p1.physicsBody?.categoryBitMask = 1
            p2.physicsBody?.categoryBitMask = 1
            p1.physicsBody?.collisionBitMask = 2 | 3
            p2.physicsBody?.collisionBitMask = 2 | 3
            p1.physicsBody?.isDynamic = false
            p2.physicsBody?.isDynamic = false
            p1.physicsBody?.friction = 0
            p1.physicsBody?.restitution = 1
            p2.physicsBody?.friction = 0
            p2.physicsBody?.restitution = 1
        
        self.addChild(p1)
        self.addChild(p2)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t: AnyObject in touches{
            let touchpos = t.location(in: self)
            let prevtouchpos = t.previousLocation(in: self)
            let movement = touchpos.x - prevtouchpos.x
            if(touchpos.y < self.frame.size.height/2)
            {
            p1.position.x += movement
            }
            else{
            p2.position.x += movement
            }
        }
    }
    
    /*func touchDown(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.green
            self.addChild(n)
        }
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.blue
            self.addChild(n)
        }
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.red
            self.addChild(n)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let label = self.label {
            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
        }
        
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }*/
    
    
    override func update(_ currentTime: TimeInterval) {
        if ball.position.y < p1.position.y - 10{
        ball.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2 + 7)
        ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        score2 += 1
            if score2 > halfway  && score2 < final5 {
            scorelabel2.fontColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            }
            if score2 >= final5
            {
                scorelabel2.fontColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }
            scorelabel2.text = "\(score2)"
            scorelabel2.run(bouncybouncy)
            ball.physicsBody?.applyImpulse(CGVector(dx: 10, dy: -10))
        }
        else if ball.position.y > p2.position.y + 10{
            ball.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2 + 7)
            ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            score1 += 1
            if score1 > halfway && score1 < final5 {
                scorelabel1.fontColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            }
            if score1 >= final5
            {
                scorelabel1.fontColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }

            scorelabel1.text = "\(score1)"
            scorelabel1.run(bouncybouncy)
            ball.physicsBody?.applyImpulse(CGVector(dx: -10, dy: 10))
        }
        if ball2.position.y < p1.position.y - 10{
            ball2.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2 + 7)
            ball2.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            score2 += 1
            if score2 > halfway && score2 < final5 {
                scorelabel2.fontColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            }
            if score2 >= final5
            {
                scorelabel2.fontColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }
            scorelabel2.text = "\(score2)"
            scorelabel2.run(bouncybouncy)
            ball2.physicsBody?.applyImpulse(CGVector(dx: 11, dy: -11))
        }
        else if ball2.position.y > p2.position.y + 10{
            ball2.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2 + 7)
            ball2.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            score1 += 1
            if score1 > halfway && score1 < final5 {
                scorelabel1.fontColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            }
            if score1 >= final5
            {
                scorelabel1.fontColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }
            
            scorelabel1.text = "\(score1)"
            scorelabel1.run(bouncybouncy)
            ball2.physicsBody?.applyImpulse(CGVector(dx: -11, dy: 11))
        }
        if (score1 == targt && score2 < targt){
            winner = 1
        let toScene = GOScene(size: self.size)
        toScene.scaleMode = self.scaleMode
        let move = SKTransition.fade(withDuration: 1)
        self.view!.presentScene(toScene, transition: move)
        }
        else if (score2 == targt && score1 < targt){
            winner = 2
            let toScene = GOScene(size: self.size)
            toScene.scaleMode = self.scaleMode
            let move = SKTransition.fade(withDuration: 1)
            self.view!.presentScene(toScene, transition: move)
        }
    }
}
